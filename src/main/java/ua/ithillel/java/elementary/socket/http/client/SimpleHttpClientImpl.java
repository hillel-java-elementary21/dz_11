package ua.ithillel.java.elementary.socket.http.client;

import lombok.Builder;
import lombok.Value;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@Value
@Builder

public class SimpleHttpClientImpl implements SimpleHttpClient {

    @Override
    public SimpleHttpResponse request(HttpMethod method, String url, String payload, Map<String, String> headers) {
        SimpleHttpResponse.SimpleHttpResponseBuilder builder = SimpleHttpResponse.builder();
        try {
            URL url1 = new URL(url);
            Socket socket = new Socket(url1.getHost(), checkPort(url1));
            Scanner scanner = new Scanner(socket.getInputStream());
            PrintWriter writer = new PrintWriter(socket.getOutputStream());

            setRequest(method, payload, headers, url1, writer);
            getResponse(scanner, builder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.build();
    }

    private int checkPort(URL url) {
        int port = 80;
        if (url.getPort() != -1) port = url.getPort();
        return port;
    }

    private void setRequest(HttpMethod met, String payload, Map<String, String> headers, URL url, PrintWriter writer) {
        writer.print(met + " " + url.getPath() + " " + url.getProtocol().toUpperCase() + "/1.0\r\n");
        writer.print("Host: " + url.getHost() + "\r\n");
        setHeaders(headers, writer);
        setPayload(payload, writer);
        writer.print("\r\n");
        writer.flush();
    }

    private void setPayload(String payload, PrintWriter writer) {
        if (payload != null) {
            writer.print("Content-Length: " + payload.getBytes(StandardCharsets.UTF_8).length + "\r\n");
            writer.print("\r\n");
            writer.print(payload + "\r\n");
        }
    }

    private void setHeaders(Map<String, String> headers, PrintWriter writer) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            writer.print(entry.getKey() + ":" + entry.getValue() + "\r\n");
        }
    }

    private void getResponse(Scanner scanner, SimpleHttpResponse.SimpleHttpResponseBuilder builder) {
        String line = scanner.nextLine();
        String[] parts = line.split("\\s");
        builder.statusCode(Integer.parseInt(parts[1]));
        builder.statusText(parts[2]);
        builder.headers(getHeaders(scanner));
        builder.payload(getPayload(scanner));
    }

    private String getPayload(Scanner scanner) {
        String line;
        do {
            line = scanner.nextLine();
            if (line.isEmpty()) break;
        } while (scanner.hasNextLine());
        return line.trim();
    }

    private Map<String, String> getHeaders(Scanner scanner) {
        Map<String, String> headers = new HashMap<>();
        do {
            String line = scanner.nextLine();
            if (line.isEmpty()) break;
            String[] parts2 = line.split(":");
            headers.put(parts2[0], parts2[1].trim());
        } while (scanner.hasNextLine());
        return headers;
    }
}
